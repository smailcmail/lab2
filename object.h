#ifndef LAB2_OBJECT_H
#define LAB2_OBJECT_H
#include "string"
#include "vector"
#include "map"
#include <iostream>
#include <fstream>
#include <utility>
#include <bits/stdc++.h>

/**
 * Класс для описания любого объекта (планета, компьютер, какая-то там Кристина, какое-то там солнышко <3)
 */
class Object{
protected:
    std::map<std::string, std::string> info;
public:
    // Можем создавать объект с набором параметров
    explicit Object(std::map<std::string, std::string> info){
        this->info = std::move(info);
    }
    // Можем создавать пустой объект
    Object()= default;
    // Можем читать вектор объектов из базы данных, передавая количество параметров у объекта и его имя
    static std::vector<Object> read_db(const std::string& filename, unsigned int params){
        std::ifstream inFile(filename);
        std::vector<Object> objects;

        unsigned int counter = params;
        std::string key;
        std::string value;
        std::map<std::string, std::string> newMap;
        while (inFile >> key >> value) {
            newMap[key] = value;
            counter--;
            if (counter == 0){
                objects.emplace_back(newMap);
                counter = params;
            }
        }

        inFile.close();
        return objects;
    }
    // Можем записывать вектор в БД
    static void write_db(const std::vector<Object>& objects, const std::string& filename){
        std::ofstream outFile(filename);

        for (const auto& i : objects)
            for (const auto& [key, value] : i.info)
                outFile << key << " " << value << std::endl;

        outFile.close();

    }
    /**
     * @param objects Вектор сортируемых векторов
     * @param value Название поля, по которому сортируем (e.g. если сортируем по имени, то передаем "name")
     * @return Отсортированный вектор объектов
     */
    static std::vector<Object> sort(std::vector<Object> objects, const std::string& value){
        std::sort(objects.begin(),
                  objects.end(),
                  [value](const Object& o1, const Object& o2) {
            return o1.info.at(value).compare(o2.info.at(value));
        });
        return objects;
    }
    // Можем добавлять новый объект в вектор объектов
    static std::vector<Object> add(std::vector<Object> objects, const Object& new_object){
        objects.push_back(new_object);
        return objects;
    }
    // Можем удалять объект в векторе объектов по индексу
    static std::vector<Object> del(std::vector<Object> objects, unsigned int index){
        objects.erase(objects.begin() + index);
        return objects;
    }
    // Можем получать индекс искомого объекта (объекты равны, если совпадают их параметры)
    static int find(std::vector<Object> objects, const Object& object){
        for(int i = 0; i < objects.size(); i++) if (objects.at(i).info == object.info) return i; return -1;
    }
    // Можем редактировать объект, указывая, какой индекс мы хотим поменять, и на какой именно объект
    static std::vector<Object> edit(std::vector<Object> objects, unsigned int index, Object object){
        objects.at(index) = std::move(object);
        return objects;
    }
    // Малюем базу данных
    static void print_db(const std::vector<Object>& objects){
        for (const auto& i : objects)
        {
            for (auto const& j : i.info) std::cout << j.first << ": " << j.second << std::endl;
            std::cout << "============" << std::endl;
        }
    }
};

#endif //LAB2_OBJECT_H

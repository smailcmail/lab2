#include <iostream>
#include "object.h"

int main() {
    // Создаём две планеты
    Object planet1 = Object(std::map<std::string, std::string>({{"name", "Venera"},
                                                                {"life", "false"}}));
    Object planet2 = Object(std::map<std::string, std::string>({{"name", "Earth"},
                                                                {"life", "true"}}));
    // Планеты, которые мы запишем в файл
    std::vector<Object> write_planets;
    // Планеты, которые мы прочитаем из файла
    std::vector<Object> read_planets;
    write_planets.push_back(planet1);
    write_planets.push_back(planet2);
    // Записываем
    Object::write_db(write_planets, "database.db");
    // Читаем планеты. Так как всего два параметра (name и life), то указываем 2
    read_planets = Object::read_db("database.db", 2);

    // Выводим то, что прочитали
    Object::print_db(read_planets);
    // Сортируем
    read_planets = Object::sort(read_planets, "name");
    // После сортировки по имени Земля будет отображаться в первую очередь
    Object::print_db(read_planets);

    // Ищем планеты и получаем их индексы
    std::cout << Object::find(read_planets, planet1) << std::endl;
    std::cout << Object::find(read_planets, planet2) << std::endl;
    return 0;
}

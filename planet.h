#ifndef LAB2_PLANET_H
#define LAB2_PLANET_H
#include "object.h"
#include "cstring"

class Planet : public Object{
protected:
    char* name;
    unsigned int diameter;
    bool life;
    unsigned int satellites;
public:
    Planet(char* name, unsigned int diameter, bool life, unsigned int satellites){
        char* t_name = new char[std::strlen(name)+1];
        for (int i = 0; i < std::strlen(name); i++) { t_name[i] = name[i];}
        this->name = t_name;
        this->diameter = diameter;
        this->life = life;
        this->satellites = satellites;
    }
    ~Planet(){
        delete[] name;
    }
    // Дописать шо-то там :)
};
#endif //LAB2_PLANET_H
